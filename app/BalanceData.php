<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BalanceData extends Model
{
    use SoftDeletes;
    /**
     * このプロパティに設定したフィールドはsaveやupdateメソッドで設定が可能となります。
     *
     * @var array $fillable
     */
    protected $fillable = [
        'occurred_at', 'user_id', 'title', 'type', 'amount', 'description'
    ];

    /**
     * このメソッドでは「Userと１対多」であるというリレーションを設定しています。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    } //
    /**
     * 収支情報の日付を「yyyy年mm月dd日(w)」形式へ整形して返します。
     *
     * @return String
     */
    public function getOccurredAtFAttribute()
    {
        $date = new Carbon($this->occurred_at);
        $day_of_week = array('日', '月', '火', '水', '木', '金', '土');
        return $date->format('Y年m月d日') . '(' . $day_of_week[$date->format('w')] . ')';
    }

    /**
     * 収支情報の金額を「￥999,999」形式へ整形して返します。
     *
     * @return String
     */
    public function getAmountFAttribute()
    {
        return '￥' . number_format($this->amount) . '';
    }
}

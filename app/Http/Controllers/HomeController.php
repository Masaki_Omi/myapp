<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\BalanceDataRepository;

class HomeController extends Controller {
    protected $balance_data_repository;

    /**
     * トップページ用のコントローラークラスのコンストラクタです。
     *
     * @return void
     */
    public function __construct(BalanceDataRepository $balance_data_repository)
    {
        // ログイン周りのミドルウェアを適用します。
        // このコントローラーにアサインされるルートは全てログインが必須となります。
        $this->middleware('auth');

        // DBアクセスを行うリポジトリのインスタンスをクラス変数に保持します。
        $this->balance_data_repository = $balance_data_repository;
    }

    /**
     * メニューと収支状況グラフを表示するためのアクションです。
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // 表示対象年月のパラメータを取得します。
        // パラメータなしの場合は今日の年月を設定します。
        $graph_target_date_info = getYmParam($request, 'ym');

        // 収支状況グラフを作成します。
        $lavacharts = $this->balance_data_repository
            ->getTopPageGraph(
                $request->user(),
            $graph_target_date_info['year'],
            $graph_target_date_info['month']);

        // トップページのビューを用意します。
        $renderable = view('home');

        // トップページで使用する以下のデータをビューに渡します。
        // 1. 収支状況グラフ用のlavachartsのインスタンス
        $renderable->with('lavacharts', $lavacharts);
        $renderable->with('ym', getYmString($graph_target_date_info));
        return $renderable;
    }
}

<?php

namespace App\Http\Controllers;

use App\BalanceData;
use Collective\Html\FormBuilder;
use Illuminate\Http\Request;
use App\Repositories\BalanceDataRepository;
use App\Http\Requests\BalanceDataRequest;


class BalanceDataController extends Controller
{
    const LIST_ITEM_PER_PAGE = 10;
    protected $balance_data_repository;

    /**
     * 収支情報編集画面用のコントローラークラスのコンストラクタです。
     *
     * @param \App\Repositories\BalanceDataRepository $balance_data_repository
     * DBアクセスを行うリポジトリのインスタンスです。
     * @return void
     */
    public function __construct(BalanceDataRepository $balance_data_repository)
    {
        // ログイン周りのミドルウェアを適用します。
        // このコントローラーにアサインされるルートは全てログインが必須となります。
        $this->middleware('auth');

        // DBアクセスを行うリポジトリのインスタンスをクラス変数に保持します。
        $this->balance_data_repository = $balance_data_repository;

        // 収支情報一覧の各行に配置する操作ボタンの雛形を定義しています。
        FormBuilder::macro('CustomButton', function ($type, $func, $label, $id) {
            return '<button type="button" class="btn btn-sm btn-outline-' . $type
                . '" onclick="' . "balance_data_controller.{$func}('{$id}');"
                . '">' . $label . '</button>';
        });
    }

    /**
     * 収支情報一覧を表示するためのアクションです。
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // 表示対象年月のパラメータを取得します。
        // パラメータなしの場合は今日の年月を設定します。
        $graph_target_date_info = getYmParam($request, 'ym');
        // 収支情報一覧のデータリストを取得します。
        // 現在のユーザーに紐付く、表示対象年月内のデータを
        // LIST_ITEM_PER_PAGEに指定された件数でページングして取得します。
        $balance_data_list = $this->balance_data_repository->getListData(
            $request->user(),
            $graph_target_date_info['year'],
            $graph_target_date_info['month'],
            self::LIST_ITEM_PER_PAGE
        );
        // 新規登録時用に入力フォーム初期値用に空のモデルをインスタンス化して、
        // 日付入力欄に今日の日付をセットします。
        $balance_data = new BalanceData();
        $balance_data->occurred_at = date('Y-m-d');

        // 収支情報一覧のビューを用意します。
        $renderable = view('balancedata.index');

        // 収支情報一覧で使用する以下のデータをビューに渡します。
        // 1. 収支情報一覧のデータリスト
        // 2. 表示年月パラメータ
        // 3. 入力フォーム初期値用のモデルのインスタンス
        // 4. 入力フォームのaction属性に設定する「新規登録用」のURLを持つrouter
        $renderable->with('balance_data_list', $balance_data_list);
        $renderable->with('ym', getYmString($graph_target_date_info));
        $renderable->with('balance_data', $balance_data);
        $renderable->with('form_router', 'balancedata.create');
        return $renderable;
    }

    /** 入力フォームのデータを受け取り、DBへ保存するためのアクションです。
     *
     * @param \App\Http\Requests\BalanceDataRequest $request
     * バリデーション済みの入力フォームの内容
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(BalanceDataRequest $request) {
        // このアクションに到達している時点で入力内容のバリデーションは完了しています。
        // エラーの場合はエラーメッセージと共に「back()->withInput()」と同等の処理が行われます。
        $validated_data = $request->validated();

        // back()を使用して収支情報一覧画面へ戻ります。
        // これにより表示年月とページのパラメータは保持されたままとなります。
        $renderable = back();

        // DBへの保存に成功すると保存したデータ(Model)が返却されます。
        // 失敗した場合はfalseが返却されます。
        $balance_data = $this->balance_data_repository
                ->createData($request->user(), $validated_data);

        // 処理結果メッセージをビューに渡します。
        if ($balance_data !== false) {
            $renderable->with('message', "データの登録が完了しました。");
            $renderable->with('alert_mode', 'success');
        } else {
            $renderable->with('message', "データの登録に失敗しました。再度実行してください。");
            $renderable->with('alert_mode', 'danger');
        }

        // 成功の場合は連続登録のため、失敗の場合は再試行のために入力データを復元します。
        $renderable->withInput();
        return $renderable;
    }
    /**
     * IDを元にデータの削除を実行します。
     *
     * @param \Illuminate\Http\Request $request
     * @param string $id 削除対象の収支情報のID
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function delete(Request $request, string $id)
    {
        // 削除対象のデータを取得します。
        $balance_data = $this->balance_data_repository
            ->getDataById($request->user(), $id);

        // 削除の成否が返却されます。
        $result = $balance_data->delete();

        // back()を使用して収支情報一覧画面へ戻ります。
        // これにより表示年月とページのパラメータは保持されたままとなります。
        $renderable = back();

        // 処理結果メッセージをビューに渡します。
        // また、入力フォームには再度新規登録できる状態のデータおよびactionを設定します。
        if ($result) {
            $renderable->with('message', "データの削除が完了しました。");
            $renderable->with('alert_mode', 'success');
            unset($balance_data->id);
            $renderable->with('balance_data', $balance_data);
        } else {
            $renderable->with('message', "【ID:{$id}】 の削除に失敗しました。再度実行してください。");
            $renderable->with('alert_mode', 'danger');
        }
        return $renderable;
    }
    /**
     * 収支情報のIDを受け取り、入力フォームを変更モードにするためのアクションです。
     *
     * @param \Illuminate\Http\Request $request
     * @param String $id 変更対象とする収支情報データのIDです。
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, string $id)
    {
        // 収支情報のIDに対応するデータを取得します。
        $balance_data = $this->balance_data_repository
            ->getDataById($request->user(), $id);

        // back()を使用して収支情報一覧画面へ戻ります。
        // これにより表示年月とページのパラメータは保持されたままとなります。
        $renderable = back();

        // 収支情報一覧で使用する以下のデータをビューに渡します。
        // 1. 収支情報のIDに対応するデータ
        // 2. 入力フォームのaction属性に設定する「情報変更用」のURLを持つrouter
        $renderable->with('balance_data', $balance_data);
        $renderable->with('form_router', ['balancedata.update', $id]);
        return $renderable;
    }

    /**
     * 入力フォームのデータを受け取り、DBデータを更新するためのアクションです。
     *
     * @param App\Http\Requests\BalanceDataRequest $request
     * バリデーション済みの入力フォームの内容
     * @param string $id 更新対象の収支情報のID
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(BalanceDataRequest $request, string $id)
    {
        // このアクションに到達している時点で入力内容のバリデーションは完了しています。
        // エラーの場合はエラーメッセージと共に「back()->withInput()」と同等の処理が行われます。
        $validated_data = $request->validated();

        // back()を使用して収支情報一覧画面へ戻ります。
        // これにより表示年月とページのパラメータは保持されたままとなります。
        $renderable = back();

        // 更新の成否が返却されます。
        $result = $this->balance_data_repository
            ->updateData($request->user(), $id, $validated_data);

        // 処理結果メッセージをビューに渡します。
        // また、入力フォームには再度更新できる状態のデータおよびactionを設定します。
        if ($result) {
            $balance_data = $this->balance_data_repository
                ->getDataById($request->user(), $id);
            $renderable->with('message', "データの更新が完了しました。");
            $renderable->with('alert_mode', 'success');
            $renderable->with('balance_data', $balance_data);
            $renderable->with('form_router', ['balancedata.update', $id]);
        } else {
            $renderable->with('message', "データの更新に失敗しました。再度実行してください。");
            $renderable->with('alert_mode', 'danger');
        }
        return $renderable;
    }
}

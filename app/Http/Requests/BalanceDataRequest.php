<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BalanceDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * このリクエストを受け付けるアクションはこのメソッドに設定したルールで
     * リクエストデータのバリデーションを行い、エラーとならない場合のみ処理が継続されます。
     * エラーの場合はエラーメッセージと共に「back()->withInput()」と同等の処理が行われます。
     *
     * @return array
     */
    public function rules()
    {
        // 日付：必須、date型
        // 区分：必須、2文字
        // 金額：必須、1〜8桁の数値のみ
        // タイトル：必須、20文字以下
        // メモ：任意
        return [
            'occurred_at' => 'required|date',
            'type' => 'required|size:2',
            'amount' => 'required|digits_between:1,8',
            'title' => 'required|max:20',
            'description' => 'nullable',
        ];
    }

    // public function all($keys = null)
    // { //確認が終わったらコメントアウトしておきましょう！
    //     $results = parent::all($keys);
    //     $results['occurred_at'] = '2019-09-31'; //存在しない日付
    //     $results['type'] = '臨時収入'; //2文字以外
    //     return $results;
    // }
}

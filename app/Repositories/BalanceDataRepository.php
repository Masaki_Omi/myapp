<?php

namespace App\Repositories;

use App\User;
use Khill\Lavacharts\Lavacharts;

class BalanceDataRepository
{
    /**
     * 収支データを１件登録します。
     *
     * @param \App\User $user 収支データ登録対象ユーザー
     * @param array $valid_data BalanceDataRequestによりvalidation済みのデータ
     * @return \Illuminate\Database\Eloquent\Model|false
     */
    public function createData(User $user, array $valid_data)
    {
        return $user->balance_data()->create($valid_data);
    }
  /**
   * 収支一覧用のデータリストを取得します。
   *
   * @param \App\User $user 一覧表示対象ユーザー
   * @param int $year 一覧表示対象年月の年
   * @param int $month 一覧表示対象年月の月
   * @param int $item_per_page ページ内のデータ件数
   * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
   */
  public function getListData(User $user, int $year, int $month, int $item_per_page)
  {
    // 一覧表示対象ユーザーで、
    // 収支発生日付が表示対象年月の範囲内のデータを絞り込み
    // 収支発生日付の降順、かつ登録日の降順
    return $user->balance_data()
      ->whereYear('occurred_at', $year)
      ->whereMonth('occurred_at', $month)
      ->orderBy('occurred_at', 'desc')
      ->orderBy('created_at', 'desc')
      ->paginate($item_per_page);
  }

  /**
     * 収支状況グラフ用データを取得します。
     *
     * @param \App\User $user データ取得対象ユーザー
     * @param int $year グラフ表示対象年月の年
     * @param int $month グラフ表示対象年月の月
     * @return array
     */
    private function getTopPageGraphData(User $user, int $year, int $month) {
        // 日付と区分ごとに金額を合計します。
        $rows = $user->balance_data()
                ->select('occurred_at', 'type')
                ->selectRaw('sum(amount) as amount')
                ->whereYear('occurred_at', $year)
                ->whereMonth('occurred_at', $month)
                ->groupBy('occurred_at', 'type')
                ->orderBy('occurred_at', 'asc')
                ->orderBy('type', 'asc')
                ->get();

        // 日付と区分の2次元配列に金額を格納して返します。
        $data = [];
        foreach ($rows as $row) {
            $data[$row->occurred_at][$row->type] = $row->amount;
        }
        return $data;
    }

    /**
     * 収支状況グラフを作成します。
     *
     * @param \App\User $user グラフ表示対象ユーザー
     * @param int $year グラフ表示対象年月の年
     * @param int $month グラフ表示対象年月の月
     * @return \Khill\Lavacharts\Lavacharts | false
     */
    public function getTopPageGraph(User $user, int $year, int $month) {
        // ログインユーザーの表示対象年月のグラフ用データを取得します。
        $rows = $this->getTopPageGraphData($user, $year, $month);

        // グラフ用データが存在しない場合はfalseを返却します。
        $lavacharts = false;
        if (count($rows) > 0) {

            // グラフ作成用プラグインのインスタンスを生成します。
            $lavacharts = new Lavacharts;

            // グラフ用データテーブルを作成し、
            // 日付・収入・支出・差引の４列を追加します。
            $data = $lavacharts->DataTable();
            $formatter = $lavacharts->DateFormat([
                'pattern' => 'M月d日'
            ]);

            $data->addDateColumn('日付', $formatter)
                    ->addNumberColumn('収入')
                    ->addNumberColumn('支出')
                    ->addNumberColumn('差引');

            // データが存在する各日付ごとに収入・支出
            // および差引の累計をデータテーブルへ追加します。
            $sum = 0;
            foreach ($rows as $date => $total) {
                $plus = ($total['収入'] ?? 0);
                $minus = -($total['支出'] ?? 0);
                $sum += $plus + $minus;
                $data->addRow([$date, $plus, $minus, $sum]);
            }

            // グラフ作成用プラグインのインスタンスに対し、
            // balance_graphという名前で棒＋折れ線のグラフを作成するよう
            // 必要なデータテーブルと各種設定を定義して渡します。
            $lavacharts->ComboChart('balance_graph', $data, [
                'title' => '収支状況グラフ',
                'height' => 400,
                'hAxis' => [
                    'format' => 'yyyy/MM/dd',
                ],
                'vAxis' => [
                    'gridlines' => [
                        'count' => 8,
                    ],
                ],
                'series' => [
                    0 => [
                        'type' => 'bars',
                        'color' => '#4bbeb4'
                    ],
                    1 => [
                        'type' => 'bars',
                        'color' => '#ef7552'
                    ],
                    2 => [
                        'type' => 'line',
                        'color' => '#fe9b00',
                        'lineWidth' => 3,
                        'curveType' => 'function'
                    ]
                ]
            ]);
        }
        return $lavacharts;
    }
    /**
     * IDにより収支データを１件取得します。
     *
     * @param \App\User $user データ取得対象ユーザー
     * @param string $id データ取得対象ID
     * @return \Illuminate\Database\Eloquent\Model | null
     */
    public function getDataById(User $user, string $id)
    {
        return $user->balance_data()->find($id);
    }
    /**
     * 収支データを１件更新します。
     *
     * @param \App\User $user 収支データ登録対象ユーザー
     * @param string $id データ取得対象ID
     * @param array $valid_data BalanceDataRequestによりvalidation済みのデータ
     * @return bool
     */
    public function updateData(User $user, string $id, array $valid_data)
    {
        return $user->balance_data()->find($id)->update($valid_data);
    }
}

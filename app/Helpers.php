<?php

/**
 * 年月パラメータをrequestから取得し、日付型へ変換後、
 * 年月日時分秒が含まれる配列として返却します。
 *
 * 年月パラメータが日付型として正しくない場合には
 * 当日の日付を変換して返却します。
 *
 * @param Illuminate\Http\Request $request
 * @param string $key 取得する年月パラメータの名称
 * @return array
 */
function getYmParam(Illuminate\Http\Request $request, string $key): array
{
  $Ym = $request->get($key);
  $date_info = date_parse_from_format('Ym', $Ym);
  if (($date_info['year'] === false) || ($date_info['month'] === false)) {
    $date_info = date_parse_from_format('Ym', date('Ym'));
  }
  return $date_info;
}

/**
 * 年月日時分秒が含まれる配列から年月パラメータを作成して返却します。
 *
 * @param array $date_info 年月日時分秒が含まれる配列
 * @return string
 */
function getYmString(array $date_info): string
{
  return $date_info['year'] . str_pad($date_info['month'], 2, '0', STR_PAD_LEFT);
}

/**
 * 指定した年月の範囲で、年月選択プルダウン用の二次元連想配列を返却します。
 * 一次元目はグルーピングと含まれるoption、
 * 二次元目は各optionのvalueとinnerTextです。
 *
 * @param string $Ym | null defaults null
 * @param int $offset_month_before 基準年月からプルダウンの先頭年月を算出するためのオフセット
 * @param int $offset_month_after 基準年月からプルダウンの末尾年月を算出するためのオフセット
 * @return string
 */
function getYmOptions(
  string $Ym = null,
  int $offset_month_before = 12,
  int $offset_month_after = 12
): array {
  $options = [];
  $Ym = $Ym ?? date('Ym');
  $disp_month = date_create_from_format('Ym', $Ym);
  $prev_month = (clone $disp_month)->modify("-1 months");
  $next_month = (clone $disp_month)->modify("+1 months");
  $curr_month = date_create_from_format('Ym', date('Ym'));

  $options['表示年月'] = [$disp_month->format('Ym') => $disp_month->format('Y年m月')];
  $options['前月'] = [$prev_month->format('Ym') => $prev_month->format('Y年m月')];
  $options['次月'] = [$next_month->format('Ym') => $next_month->format('Y年m月')];
  $options['今月'] = [$curr_month->format('Ym') => $curr_month->format('Y年m月')];

  $start_month = (clone $disp_month)->modify("-{$offset_month_before} months");
  $end_month = (clone $disp_month)->modify("+{$offset_month_after} months");
  $interval = new DateInterval('P1M');
  $period = new DatePeriod($start_month, $interval, $end_month);

  $option = [];
  foreach ($period as $month) {
    $option[$month->format('Ym')] = $month->format('Y年m月');
  }
  $options['年月指定'] = $option;

  return $options;
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::redirect('/', '/home');
Route::get('/home', 'HomeController@index')->name('home');
Route::prefix('balancedata')->name('balancedata.')->group(function () {
    Route::get('/', 'BalanceDataController@index')->name('index'); // (1)
    Route::post('/create', 'BalanceDataController@create')->name('create'); // (2)
    Route::get('/edit/{id}', 'BalanceDataController@edit')->name('edit'); // (3)
    Route::post('/update/{id}', 'BalanceDataController@update')->name('update'); // (4)
    Route::get('/delete/{id}', 'BalanceDataController@delete')->name('delete'); // (5)
});

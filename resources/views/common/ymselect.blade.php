{{ Form::open(['method' => 'GET']) }}
<div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text">年月</span>
  </div>
  {{ Form::select('ym', getYmOptions($ym), null, [
        'class' => 'form-control',
        'onchange' => 'submit(this.form);']) }}
  {{ Form::button('再読込', [
        'class' => 'btn btn-outline-success ml-1',
        'onclick' => 'location.reload();']) }}
</div>
{{ Form::close() }}

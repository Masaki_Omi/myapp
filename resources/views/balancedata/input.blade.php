@if (session('message'))
<div class="alert alert-{{ session('alert_mode') }}">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  {!! nl2br(htmlspecialchars(session('message'))) !!}
</div>
@endif
<div class="card">
  <div class="card-header">収支情報編集フォーム</div>
  <div class="card-body">
    {{ Form::model(session('balance_data') ?? $balance_data,[
                'route' => session('form_router') ?? $form_router])}}
    <div class="form-row">
      <div class="mb-3 col-4 col-sm-4 col-md-4 col-lg-2 col-xl-2">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">ＩＤ</span>
          </div>
          {{ Form::text('id', null, [
                            'class' => 'form-control text-center','readonly']) }}
        </div>
      </div>
      <div class="mb-3 col-8 col-sm-8 col-md-8 col-lg-4 col-xl-4">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">日付</span>
          </div>
          {{ Form::date('occurred_at', null, [
                            'class' => 'form-control text-center rounded-right'
                            . ($errors->has('occurred_at') ? ' is-invalid' : ''),
                            'required']) }}
          @if ($errors->has('occurred_at'))
          <div class="invalid-feedback">
            <strong>{{ $errors->first('occurred_at') }}</strong>
          </div>
          @else
          <div class="invalid-feedback">
            <strong>日付は必ず指定してください。</strong>
          </div>
          @endif
        </div>
      </div>

      <div class="mb-3 col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">金額</span>
          </div>
          {{ Form::select('type', ['収入' => '収入', '支出' => '支出'], null, [
                            'class' => 'form-control'
                            . ($errors->has('type') ? ' is-invalid' : '')]) }}
          {{ Form::text('amount', null, [
                            'class' => 'form-control text-right'
                            . ($errors->has('amount') ? ' is-invalid' : ''),
                            'required', 'pattern' => '\d{1,8}']) }}
          <div class="input-group-append">
            <span class="input-group-text rounded-right">円</span>
          </div>
          @if ($errors->has('type') || $errors->has('amount'))
          <div class="invalid-feedback">
            <strong>{{ $errors->first('type') }}</strong>
            <strong>{{ $errors->first('amount') }}</strong>
          </div>
          @else
          <div class="invalid-feedback">
            <strong>金額は1桁から8桁の間で指定してください。</strong>
          </div>
          @endif
        </div>
      </div>

      <div class="mb-3 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">タイトル</span>
          </div>
          {{ Form::text('title', null, [
                            'class' => 'form-control rounded-right'
                            . ($errors->has('title') ? ' is-invalid' : ''),
                            'required','pattern' => '{1,20}']) }}
          @if ($errors->has('title'))
          <div class="invalid-feedback">
            <strong>{{ $errors->first('title') }}</strong>
          </div>
          @else
          <div class="invalid-feedback">
            <strong>タイトルは必ず指定してください。</strong>
          </div>
          @endif
        </div>
      </div>

      <div class="mb-3 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">メモ</span>
          </div>
          {{ Form::textarea('description', null, [
                            'class' => 'form-control', 'rows' => 3]) }}
        </div>
      </div>
    </div>
    <div class="text-right">
      {{ Form::button('クリア', [
                    'class' => 'btn btn-outline-danger',
                    'onclick' => 'location.reload();']) }}
      {{ Form::submit('送信', ['class' => 'btn btn-outline-success',
                    'onclick' => 'return balance_data_controller.form_validation(this.form);']) }}
    </div>
    {{ Form::close() }}
  </div>
</div>

<div class="card mt-4">
  <div class="card-header">収支状況グラフ</div>
  <div class="card-body">
    @include('common.ymselect')
    <div id="charts_canvas">
      @if ($lavacharts !== false)
      {!! $lavacharts->render('ComboChart', 'balance_graph', 'charts_canvas'); !!}
      @else
      <div class="alert alert-info mt-3" role="alert">データがありません</div>
      @endif
    </div>
  </div>
</div>

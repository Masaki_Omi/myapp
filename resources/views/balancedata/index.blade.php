{{-- @extends('layouts.app')により、実際にはアプリケーションで共通の部分を持つ --}}
{{-- 「resources/views/layouts/app.blade.php」が最初に読み込まれ、 --}}
{{-- その中の@yield('content')によりこのファイルの中身が読み込まれます。 --}}
@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      @include('balancedata.input')
      @include('balancedata.list')
    </div>
  </div>
</div>
@endsection

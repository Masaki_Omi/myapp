<div class="card mt-4">
  <div class="card-header">収支情報一覧</div>
  <div class="card-body">
    @include('common.ymselect')
    @if (count($balance_data_list) > 0)
    {{ $balance_data_list->appends(['ym'=>$ym])->links() }}
    <table class="table table-sm table-bordered table-hover">
      <thead class="thead-light">
        <tr>
          <th class="text-center">ＩＤ</th>
          <th class="text-center">日付</th>
          <th class="text-center">区分</th>
          <th class="text-center">金額</th>
          <th class="text-center">タイトル</th>
          <th class="text-center">メモ</th>
          <th class="text-center">操作</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($balance_data_list as $row)
        <tr>
          <td class="text-center">
            <div>{{ $row->id }}</div>
          </td>
          <td class="text-center">
            <div>{{ $row->occurred_at_f }}</div>
          </td>
          <td class="text-center">
            <div>{{ $row->type }}</div>
          </td>
          <td class="text-right">
            <div class="font-weight-bold
                                {{ ($row->type == '収入') ? 'text-success' : 'text-danger' }}">
              {{ $row->amount_f }}
            </div>
          </td>
          <td class="text-center">
            <div>{{ $row->title }}</div>
          </td>
          <td>
            <div>{!! nl2br(htmlspecialchars($row->description)) !!}</div>
          </td>
          <td class="text-center">
            {!! Form::CustomButton('danger', 'delete_data', '削除', $row->id) !!}
            {!! Form::CustomButton('success', 'edit_data', '変更', $row->id) !!}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    @else
    <div class="alert alert-info mt-3" role="alert">データがありません</div>
    @endif
  </div>
</div>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header">{{__('Dashboard')}}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <!-- You are logged in! -->
                    {{ Auth::user()->name }}さん、こんにちは！
                    <br>
                    <!-- <a href="./balancedata">収支情報編集画面を開く</a> -->
                    {{ link_to_route('balancedata.index', '収支情報編集画面を開く') }}
                </div>
            </div>
            @include('balancedata.graph')
        </div>
    </div>
</div>
@endsection

class balance_data_controller {
  static form_validation = form => {
    form.classList.add('was-validated');
    return form.checkValidity();
  }
  
  static delete_data = id => {
    if (confirm('本当に 【ID:' + id + '】 を削除してよろしいですか？')) {
      location.href = '/balancedata/delete/' + id;
    }
  }

  static edit_data = id => {
    location.href = '/balancedata/edit/' + id;
  }
}

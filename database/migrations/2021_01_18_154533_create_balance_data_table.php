<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_data', function (Blueprint $table) {
            // ＩＤ（自動採番）
            $table->bigIncrements('id');
            // ユーザーＩＤ
            $table->integer('user_id')->unsigned()->index();
            // 日付
            $table->date('occurred_at');
            // タイトル
            $table->string('title', 20);
            // 区分
            $table->string('type', 2);
            // 金額
            $table->integer('amount');
            // メモ
            $table->text('description')->nullable();
            // 作成日（created_at）と更新日（updated_at）
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_data');
    }
}

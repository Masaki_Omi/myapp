<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BalanceData;
use Faker\Generator as Faker;

$factory->define(BalanceData::class, function (Faker $faker) {
    $title = ['繰越', '給与', '株式配当', '宝くじ当選', '家賃', '食費', '沖縄ダイビング旅行', '槍ヶ岳登山旅行'];
    $type = ['収入', '収入', '収入', '収入', '支出', '支出', '支出', '支出'];
    $idx = $faker->numberBetween(0, count($title) - 1);
    return [
        // テスト登録してあるユーザーの人数分のuser_idをランダムに（例では2人分）
        'user_id' => $faker->numberBetween(1, 2),
        // 現在を中心に3ヶ月程度の範囲でランダムに
        'occurred_at' => $faker->dateTimeBetween('2019-11-01', '2020-01-31'),
        // タイトルと対応する区分は配列からランダムに
        'title' => $title[$idx],
        'type' => $type[$idx],
        // 金額は1円から999,999円の範囲でランダムに
        'amount' => $faker->numberBetween(1, 999999),
        // メモは任意の文字列20文字程度
        'description' => $faker->realText(20)
    ];
});

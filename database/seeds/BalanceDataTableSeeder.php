<?php

use Illuminate\Database\Seeder;

class BalanceDataTableSeeder extends Seeder
{
    /**
     * balance_dataテーブルの初期値設定を行います。
     *
     * @return void
     */
    public function run()
    {
        // テーブル内を空にする
        DB::table('balance_data')->truncate();
        // factoryによってBalanceDataを5千個作成⇒挿入する
        factory(App\BalanceData::class, 5000)->create();
    }
}

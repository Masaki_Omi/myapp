<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * アプリケーション内の各テーブルの初期値設定を行います。
     *
     * @return void
     */
    public function run()
    {
        // balance_dataテーブルの初期値設定
        $this->call(BalanceDataTableSeeder::class);
    }
}
